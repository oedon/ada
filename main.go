package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type Crypto struct {
	Title map[string]float64 `json:"cardano"`
}

func main() {
    // Cardano API call to coingecko
	url := "https://api.coingecko.com/api/v3/simple/price?ids=cardano&vs_currencies=usd"
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

    // Get Body from response
	body, _ := ioutil.ReadAll(resp.Body)

    // Unmarshal json into struct
	var data Crypto
	err = json.Unmarshal(body, &data)
	if err != nil {
		log.Fatal(err)
	}

    // Get quote from map
	quote := data.Title
	fmt.Printf("🪙 $%.4v", quote["usd"])
}
